
# My Flutter Card

## Our Goal

Created a flutter app entirely from scratch,
and designed user interface for this name card app.

## What was created

My Flutter Card is a personal business card. Imagine every time I wanted to give someone my contact details or my business card and I didn't have it on me. Well, now they can get to download my business card as an app.

## Reflection

* Created Stateless Widgets
* Used Containers to lay out your UI
* Used Columns and Rows to position the UI elements
* Added custom fonts
* Added Material icons
* Styled Text widgets
* Read and used Flutter Documentation

## Author

* Khairul: thekhairul@yandex.com


